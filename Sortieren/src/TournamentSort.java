import java.util.Random;
import java.util.stream.Stream;

public class TournamentSort extends Sort {
	public TournamentSort(int length) {
		super(length);
	}

	public void sort() {
		// this.numbers = new Integer[] {4, 3, 7, 5, 9, 8, 0, 2, 1, 6};
		this.numbers = recursiveSort(this.numbers);

	}

	public Integer[] recursiveSort(Integer[] derivedArray) {
		int length = derivedArray.length;
		if (length == 1 || length == 0) {
			return derivedArray;
		}
		int current = derivedArray[length - 1];
		int counter = 0;
		for (int i = 0; i < length - 1; i++) {
			if (derivedArray[i] <= current) {
				counter++;
			}
		}
		Stream.of(derivedArray).limit(derivedArray.length - 1).filter(e -> e <= current).count();
		Integer[] firstArray = new Integer[counter];
		Integer[] secondArray = new Integer[length - counter - 1];
		fillArray(firstArray, derivedArray, (e) -> e <= current ? 0 : -1);
		fillArray(secondArray, derivedArray, (e) -> e > current ? 0 : -1);
		firstArray = recursiveSort(firstArray);
		secondArray = recursiveSort(secondArray);
		Integer[] currentArray = new Integer[] { current };
		Integer[] result = Stream.of(firstArray, currentArray, secondArray).flatMap(Stream::of).toArray(Integer[]::new);
		return result;

	}

	public void fillArray(Integer[] toFill, Integer[] derivedArray, Comparable<Integer> c) {
		int counter = 0;
		for (int i = 0; i < derivedArray.length - 1; i++) {
			int result = c.compareTo(derivedArray[i]);
			if (result == 0) {
				toFill[counter] = derivedArray[i];
				counter++;
			}
		}
	}
}
