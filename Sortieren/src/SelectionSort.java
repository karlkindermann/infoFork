public class SelectionSort extends Sort{
    public SelectionSort(int length) {
        super(length);
    }
    public void sort(){
        System.out.println("unsorted");
        //this.printNumbers();
        for (int i = 0; i < numbers.length-1; i++){
            int current = numbers[i];
            int min = current;
            int index = i;
            for (int j = i+1; j < numbers.length; j++){
                if (numbers[j] < min){
                    min = numbers[j];
                    index = j;
                }
            }
            numbers[index] = current;
            numbers[i] = min;
        }
        System.out.println("sorted");
        //this.printNumbers();
    }
}
