public class InsertionSort extends Sort {
    public InsertionSort(int length) {
        super(length);
    }

    public void sort() {
        System.out.println("unsorted");
        //this.printNumbers();
        for (int i = 0; i < numbers.length-1; i++){
            int small = numbers[i+1];
            for (int j  = i+1; j > 0; j--) {
                if (numbers[j-1] > small){
                    numbers[j] = numbers[j - 1];
                }
                else {
                    numbers[j] = small;
                    break;
                }
                if (j-1 == 0){
                    numbers[j-1] = small;
                    break;
                }
            }
        }
        System.out.println("sorted");
        //this.printNumbers();
    }
}
