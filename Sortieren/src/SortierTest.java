public class SortierTest {
    public static void main(String[] args) {
        //Sort sortiere = new InsertionSort(100000);
        //Sort sortiere1 = new SelectionSort(10000);
        Sort sortiere2 = new InsertionSortSayan(100000);
        //sortTest(sortiere1);
        //sortTest(sortiere);
        sortTest(sortiere2);
    	TournamentSort tSort = new TournamentSort(100000);
    	sortTest(tSort);
    }
    public static void sortTest(Sort sortiere) {
    	long startTime = System.currentTimeMillis();
    	sortiere.sort();
        long endTime = System.currentTimeMillis();
        System.out.println("Das Sortierverfahren benötigte:");
        System.out.print(endTime- startTime);
        System.out.println(" ms");
    }
}
