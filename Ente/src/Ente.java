public abstract class Ente{
  private boolean kannSchwimmen;
  private boolean kannFliegen;
  private String typ;
  private String ton;
  public Ente(boolean swim, boolean fly, String type, String sound){
    kannSchwimmen = swim;
    kannFliegen = fly;
    typ = type;
    ton = sound;
  }
  public void flieg(){
    if (kannFliegen == true) {
      System.out.println("Ich kann fliegen!");
      } 
    else{
      System.out.println("Ich kann nicht fliegen!");
    }
  }
  public void schwimm(){
    if (kannSchwimmen == true) {
      System.out.println("Ich kann schwimmen!");
    } // end of if
    else {
      System.out.println("Ich kann nicht schwimmen!");
    } // end of if-else
  }
  public void quak(){
    if (ton == "") {
      ton = "Ich bin stumm";
    } // end of if
    System.out.println(ton);
  }
  public void gibTyp(){
    System.out.println(typ);
  }
}
  