public class EnteTest{
  public static void main(String[] args){
    Stockente stockE1 = new Stockente();
    Schwan Schwan1 = new Schwan();
    Rotkopfente RotkopfE1 = new Rotkopfente();
    Ente[] EntenListe = {stockE1, Schwan1, RotkopfE1};
    for (int i = 0; i < EntenListe.length; i++) {
      Ente e = EntenListe[i];
      e.gibTyp();
      e.flieg();
      e.schwimm();
      e.quak();
    } // end of for
  }
}