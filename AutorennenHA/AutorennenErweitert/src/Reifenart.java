public class Reifenart {
	public int width;
	public int height;
	public int inch;
	public boolean winter;
	public String Producer;
	public Reifenart(String pProducer, int pWidth, int pHeight, int pInch, boolean pWinter){
		width = pWidth;
		height = pHeight;
		inch = pInch;
		winter = pWinter;
		Producer = pProducer;
	}
	public int getBreite(){
		return width;
	}
	public int getHoehe(){
		return height;
	}
	public int getZoll(){
		return inch;
	}
	public boolean Winterfest(){
		return winter;
	}
	public void setBreite(int pWidth){
		width = pWidth;
	}
	public void setHoehe(int pHeight){
		height = pHeight;
	}
	public void setZoll(int pInch){
		inch = pInch;
	}
	public void setWinter(boolean pWinter){
		winter = pWinter;
	}
	public String getProducer(){
		return Producer;
	}
	public void setProducer(String pProducer){
		Producer = pProducer;
	}
}
