import java.util.Scanner;
public class AutoTest {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		String choice = "";
		String change = "";
		Reifenart Standard = new Reifenart("Apollo", 500, 120, 12, true);
		Motor VW = new Motor(4, "VW", false);
		System.out.println("Erstelle neues Auto...");
		System.out.println("Wieviel PS soll es haben?");
		int ps1 = scan.nextInt();
		Auto Auto1 = new Auto(ps1, VW, Standard);
		Auto pick = Auto1;
		System.out.println("Das erste Auto wurde erstellt!");
		System.out.println("Es verf�gt �ber " + ps1 + " PS\n");
		System.out.println("Erstelle zweites Auto...");
		System.out.println("Wieviel PS soll es haben?");
		int ps2 = scan.nextInt();
		Auto Auto2 = new Auto(ps2, VW, Standard);
		System.out.println("Das zweite Auto wurde erstellt!");
		System.out.println("Es verf�gt �ber "+ ps2 + " PS!");
		System.out.println("\n\n\n");
		if (ps1 > ps2){
			pick = Auto2;
			choice = "zweite Auto!";
			change = "erste Auto!";
		}
		else {
			choice = "erste Auto!";
			change = "zweite Auto!";
		}
		Fahrer Fahrer1 = new Fahrer("Henning", 30, pick);
		System.out.println("Fahrer 'Henning' erstellt.");
		System.out.println("Henning w�hlt das " + choice);
		System.out.println("\nUnd los...\n");
		while (Fahrer1.getSpeed() < 80){
			Fahrer1.getFahrzeug().gasgeben();
		}
		System.out.println(Fahrer1.vehicle.toString());
		System.out.println("\n\n\n");
		System.out.println("Henning will mehr PS!");
		if (pick == Auto1){
			pick = Auto2;
		}
		else{
			pick = Auto1;
		}
		Fahrer1.setFahrzeug(pick);
		System.out.println("Henning f�hrt nun das " + change);
		System.out.println("\nUnd Vollgas!\n");
		while (Fahrer1.getSpeed() < 160){
			Fahrer1.getFahrzeug().gasgeben();
		}
		System.out.println(Fahrer1.vehicle.toString());
		System.out.println("\n\n\n");
		System.out.println("Er will noch schneller werden...");
		System.out.println("\n---Kaufe neue Reifen und Motor---");
		Reifenart Rennreifen = new Reifenart("Pirelli", 615, 150, 15, false);
		Motor Benz = new Motor(6, "Benz", true);
		Fahrer1.Boxenstop(Benz, Rennreifen);
		while (Fahrer1.getSpeed() < 250){
			Fahrer1.getFahrzeug().gasgeben();
		}
		System.out.println(Fahrer1.getFahrzeug().toString());
		System.out.println("\nDamit ist der Sieg sicher!");
		scan.close();
	}
}