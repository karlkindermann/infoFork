
public class Auto {
	public int PS;
	public int Speed;
	public int Gang;
	public Motor aktMotor;
	public Reifenart aktReifen;
	public Auto(int pPS, Motor pMotor, Reifenart pReifen){
		PS = pPS;
		Gang = 1;
		Speed = 0;
		aktMotor = pMotor;
		aktReifen = pReifen;
	}
	public void bremsen(){
		if (Speed>0){
			Speed-=1;
		}
		if (Speed == 20 || Speed == 40 || Speed ==  70 || Speed ==  90){
			Gang-=1;
		}
	}
	public void gasgeben(){
		Speed+=1;
		if (Speed == 20 || Speed == 40 || Speed ==  70 || Speed ==  90){
			Gang+=1;
		}
	}
	public String toString(){
		return (
				"Der aktuelle Status des Wagens ist: \n" +
				"PS: " + PS + "\n" +
				"Geschwindigkeit: " + Speed + "\n" +
				"Gang: " + Gang + "\n" +
				"Motor: " + aktMotor.getProducer() + "\n" +
				"Reifen: " + aktReifen.getProducer());
				
	}
	public Motor getMotor(){
		return aktMotor;
	}
	public Reifenart getReifen(){
		return aktReifen;
	}
	public void setMotor(Motor pMotor){
		aktMotor = pMotor;
	}
	public void setReifen(Reifenart pReifen){
		aktReifen = pReifen;
	}
	
}
