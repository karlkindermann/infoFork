public class Motor {
	public int Zylinder;
	public boolean Benziner;
	public String Producer;
	public Motor(int pZylinder, String pProducer, boolean pBenziner){
		Zylinder = pZylinder;
		Benziner = pBenziner;
		Producer = pProducer;
	}
	public int getZylinder(){
		return Zylinder;
	}
	public boolean getBenziner(){
		return Benziner;
	}
	public void setBenziner(boolean pBenziner){
		Benziner = pBenziner;
	}
	public void setZylinder(int pZylinder){
		Zylinder = pZylinder;
	}
	public String getProducer(){
		return Producer;
	}
	public void setProducer(String pProducer){
		Producer = pProducer;
	}
	
}
