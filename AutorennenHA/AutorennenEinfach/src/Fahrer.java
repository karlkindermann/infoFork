public class Fahrer {
	public String name;
	public int age;
	public Auto vehicle;
	public Fahrer(String pName, int pAge, Auto pAuto){
		name = pName;
		age = pAge;
		vehicle = pAuto;
	}
	public String getName(){
		return name;
	}
	public int getAlter(){
		return age;
	}
	public Auto getFahrzeug(){
		return vehicle;
	}
	public void setName(String pName){
		name = pName;
	}
	public void setAlter(int pAlter){
		age = pAlter;
	}
	public void setFahrzeug(Auto pAuto){
		vehicle = pAuto;
	}
	public int getSpeed(){
		return vehicle.Speed;
	}
	public int getGang(){
		return vehicle.Gang;
	}
		
}
