import java.util.Scanner;
public class AutoTest {
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		String choice = "";
		String change = "";
		System.out.println("Erstelle neues Auto...");
		System.out.println("Wieviel PS soll es haben?");
		int ps1 = scan.nextInt();
		Auto Auto1 = new Auto(ps1);
		Auto pick = Auto1;
		System.out.println("Das erste Auto wurde erstellt!");
		System.out.println("Es verf�gt �ber " + ps1 + " PS");
		System.out.println("Erstelle zweites Auto...");
		System.out.println("Wieviel PS soll es haben?");
		int ps2 = scan.nextInt();
		Auto Auto2 = new Auto(ps2);
		System.out.println("Das zweite Auto wurde erstellt!");
		System.out.println("Es verf�gt �ber "+ ps2 + " PS!");
		if (ps1 > ps2){
			pick = Auto2;
			choice = "zweite Auto!";
			change = "erste Auto!";
		}
		else {
			choice = "erste Auto!";
			change = "zweite Auto!";
		}
		Fahrer Fahrer1 = new Fahrer("Henning", 30, pick);
		System.out.println("Fahrer 'Henning' erstellt.");
		System.out.println("Henning w�hlt das " + choice);
		System.out.println("Doch Henning will mehr PS!");
		Fahrer1.setFahrzeug(Auto1);
		System.out.println("Henning f�hrt nun das " + change);
		scan.close();
	}
}
