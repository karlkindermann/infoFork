
public class Schoko extends Zutat {

	public Schoko(Getraenk getraenk) {
		super(getraenk);
		this.description = "Schokolade";
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return this.getraenk.description + " mit " + this.description;
	}

	@Override
	public double cost() {
		// TODO Auto-generated method stub
		return this.getraenk.cost() + 0.5;
	}

}
