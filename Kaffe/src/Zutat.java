
public abstract class Zutat extends Getraenk {
	Getraenk getraenk;
	public abstract String getDescription();
	public Zutat(Getraenk getraenk) {
		this.getraenk = getraenk;
	}

}
