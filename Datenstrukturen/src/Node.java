
public class Node {
	private Node next;
	private Object content;
	public Node(Node next) {
		this.next = next;
		this.content = null;
	}
	public Node(Node next, Object content) {
		this.next = next;
		this.content = content;
	}
	public Node getNext() {
		return next;
	}
	public void setNext(Node next) {
		this.next = next;
	}
	public Object getContent() {
		return content;
	}
	public void setContent(Object content) {
		this.content = content;
	}
	public boolean isEmpty() {
		if (this.content == null) {
			return true;
		}
		return false;
	}
}
