public class List {
	private Node first;
	private Node last;
	public Node getFirst() {
		return first;
	}
	public void setFirst(Node first) {
		this.first = first;
	}
	public Node getLast() {
		return last;
	}
	public void setLast(Node last) {
		this.last = last;
	}
	public List() {
		first = new Node(null);
		last = first;
	}
	public List(Object[] objects) {
		first = new Node(null);
		this.last = first;
		for (Object o : objects) {
			this.add(o);
		}
	}
	public boolean isEmpty() {
		if (first.getContent() == null) {
			return true;
		}
		return false;
	}
	public void add(Object o) {
		/*Node n = first;
		while(n.getNext() != null) {
			n = n.getNext();
		}*/
		if (this.getFirst().isEmpty()) {
			this.getFirst().setContent(o);
			return;
		}
		Node n = new Node(null, o);
		this.getLast().setNext(n);
		this.setLast(n);
	}
	public void remove(Node current) {
		getPrevious(current).setNext(current.getNext());
	}
	public void insert(Node prev, Node toAdd) {
		if (prev == null) {
			toAdd.setNext(this.getFirst());
			this.setFirst(toAdd);
			return;
		}
		toAdd.setNext(prev.getNext());
		prev.setNext(toAdd);
	}
	public void print() {
		Node current = this.first;
		while (current != null) {
			System.out.println(current.getContent());
			current = current.getNext();
		}
	}
	public void InsertionSort() {
		sort(this.first.getNext());
	}
	public void sort(Node current) {
		if (current == null) {
			return;
		}
		int value = (int) current.getContent();
		Node prev = getPrevious(current);
		Node next = current.getNext();
		remove(current);
		current.setNext(null);
		Node temp = current;
		while (prev != null) {
			temp = prev;
			int pValue = (int) prev.getContent();
			if (pValue <= value) {
				break;
			}
			prev = getPrevious(prev);
		}
		if (prev == null) {
			temp = prev;
		}
		insert(temp, current);
		sort(next);
	}
	public Node getPrevious(Node current) {
		Node temp = this.first;
		if (current == temp) {
			return null;
		}
		while (temp.getNext() != current) {
			temp = temp.getNext();
		}
		return temp;
	}
}
