
public class NatuerlicheZahlBerechnen {
	public static void main(String[] args) {
		int n =32;
		long result;
		result = iterativ(n);
		long Result = 1;
		Result = rekursiv(n);
		System.out.println(result);
		System.out.println(Result);
	}
	public static long iterativ(int n) {
		long result = 1;
		for (int i = 1; i < n; i++) {
			result*=i;
		}
		return result*n;
	}
	public static long rekursiv(int n) {
		if (n == 1) {
			return 1;
		}
		else {
			return rekursiv(n-1)*n;
		}
	}
}
