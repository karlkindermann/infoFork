public class Rangierbahnhof {
	public Stack<Train> trackA, trackB, trackC;
	public Rangierbahnhof() {
		trackA = new Stack<Train>();
		trackB = new Stack<Train>();
		trackC = new Stack<Train>();
	}
	public void sortAscending() {
		sort(trackA, trackC, trackB);
	}
	public void sort(Stack<Train> source, Stack<Train> dest, Stack<Train> clipboard) {
		if (source.isEmpty()) {return;}
		Train current = source.getContent();
		int currentNumber = current.getNumber();
		if(dest.isEmpty() || dest.getContent().getNumber() < currentNumber) {
			pushTrainToTrack(source, dest);
			sort(source, dest, clipboard);
		} else {
			insertTrain(dest, clipboard, current);
			source.pop();
			sort(source, dest, clipboard);
		}
	}
	public void insertTrain(Stack<Train> dest, Stack<Train> clip, Train current) {
		while(!dest.isEmpty()&& dest.getContent().getNumber() > current.getNumber()) {
			pushTrainToTrack(dest, clip);
		}
		dest.add(current);
		while(!clip.isEmpty()) {
			pushTrainToTrack(clip, dest);
		}
	}
	public void pushTrainToTrack(Stack<Train> source, Stack<Train> dest) {
		dest.add(source.getContent());
		source.pop();
	}
	public void print() {
		System.out.println("Track A: ");
		listTrack(this.trackA);
		System.out.println("Track B: ");
		listTrack(this.trackB);
		System.out.println("Track C: ");
		listTrack(this.trackC);
	}
	public void listTrack(Stack<Train> track) {
		track.toLast();
		Train last = track.getContent();
		track.toFirst();
		Train current = null;
		while (current != last) {
			current = track.getContent();
			System.out.println(current.getNumber());
			track.getNext();
		}
		track.toLast();
	}
	//TODO: sortieren fertig machen
}
