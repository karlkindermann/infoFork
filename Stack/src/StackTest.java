public class StackTest {
    public static void main(String[] args) {
        Rangierbahnhof test = new Rangierbahnhof();
        test.trackA.add(new Train(16));
        test.trackA.add(new Train(11));
        test.trackA.add(new Train(15));
        test.trackA.add(new Train(13));
        test.trackA.add(new Train(14));
        test.print();
        test.sortAscending();
        test.print();
    }
}
