public class Stack<ContentType>{
    public class Layer{
    private ContentType content;
    private Layer next;
    public Layer(ContentType content, Layer next){
        this.content = content;
        this.next = next;
    }

        public void setContent(ContentType content) {
            this.content = content;
        }

        public void setNext(Layer next) {
            this.next = next;
        }

        public ContentType getContent() {
            return content;

        }

        public Layer getNext() {
            return next;
        }
    }
    private Layer first, last, current;
    public Stack(){
        this.current = null;
        this.first = null;
        this.last = null;
    }
    public boolean isEmpty(){
        return first == null;
    }
    public boolean hasAccess(){
        return current != null;
    }
    public void toFirst(){
        if (!isEmpty()){current = first;}
    }
    public void toLast(){
        if(!isEmpty()){current = last;}
    }
    public void getNext(){
        current = current.getNext();
    }
    public ContentType getContent(){
        if (hasAccess()) {
            return current.getContent();
        } else{return null;}
    }
    public void setContent(ContentType pContent){
        if(pContent != null && hasAccess()){
            current.setContent(pContent);
        }
    }
    public void add(ContentType pContent){
        if (pContent != null){
            Layer pLayer = new Layer(pContent, null);
            if (isEmpty()){
                this.first = pLayer;
                this.last = pLayer;
                toFirst();
            } else {
	            this.last.setNext(pLayer);
	            this.last = pLayer;
	            toLast();
            }
        }
    }
    public void pop() {
        if (!isEmpty()) {
        	toFirst();
            while(current.getNext()!= this.last) {
            	if (current.getNext() == null) {
                	this.first = null;
                	this.last = null;
                	this.current = null;
                	return;
                }
            	this.getNext();
            }
            if (current.getNext() == null) {
            	this.first = null;
            	this.last = null;
            	return;
            }
            current.setNext(null);
            this.last = current;
        }
    }
    public void concat(Stack pStack){
        while (!pStack.isEmpty()){
        	pStack.toLast();
            this.add((ContentType) pStack.getContent());
            pStack.pop();
        }
    }
    public void print(){
        toFirst();
        while (current != last){
            System.out.println(getContent());
            this.getNext();
        }
        System.out.println(last.getContent());
    }
}
