
public class Wetterstation {
	double pressure;
	double temperature;
	double humidity;
	private Kunde[] Kunden;
	private int counter;
	public Wetterstation(double pPress, double pTemp, double pHumi) {
		setPressure(pPress);
		setTemperature(pTemp);
		setHumidity(pHumi);
		Kunden = new Kunde[10];
		counter = 0;
	}
	public void register(Kunde pKunde) {
		int length = Kunden.length;
		if (counter >= 10) {return;}
		Kunden[counter] = pKunde;
		counter++;
		System.out.println("Kunde wurde erfolgreich registriert!");
	}
	public void setPressure(double pPress) {
		pressure = pPress;
		pushPressure();
	}
	public void setTemperature(double pTemp) {
		temperature = pTemp;
		pushTemperature();
	}
	public void setHumidity(double pHumi) {
		humidity = pHumi;
		pushHumidity();
	}
	public void pushPressure() {
		if (counter == 0) {return;}
		for (int i = 0; i < counter; i++) {
			Kunde k = Kunden[i];
			k.reactPressure();
		}
	}
	public void pushTemperature() {
		if (counter == 0){return;}
		for (int i = 0; i < counter; i++) {
			Kunde k = Kunden[i];
			k.reactTemperature();
		}
	}
	public void pushHumidity() {
		if (counter == 0){return;}
		for (int i = 0; i < counter; i++) {
			Kunde k = Kunden[i];
			k.reactHumidity();
		}
	}
}
