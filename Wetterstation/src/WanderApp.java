
public class WanderApp extends Kunde {

	public WanderApp(Wetterstation pWetterstation) {
		super(pWetterstation);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void reactPressure() {
		stelleDar("Druck", this.getWetterstation().pressure);

	}

	@Override
	public void reactTemperature() {
		stelleDar("Temperatur", this.getWetterstation().temperature);
	}

	@Override
	public void reactHumidity() {
		stelleDar("Feuchtigkeit", this.getWetterstation().humidity);
	}

	@Override
	void stelleDar(String property, double value) {
		System.out.println(property + " betr�gt " + value);
		
	}

}
