
public abstract class Kunde implements PropertyChangeListener{
	private Wetterstation aktWetterstation;
	public Kunde(Wetterstation pWetterstation) {
		setWetterstation(pWetterstation);
	}
	public void setWetterstation(Wetterstation pWetterstation) {
		aktWetterstation = pWetterstation;
		pWetterstation.register(this);
	}
	public Wetterstation getWetterstation() {
		return aktWetterstation;
	}
	abstract void stelleDar(String property, double value);
}
