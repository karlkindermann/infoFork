
public interface PropertyChangeListener {
	void reactPressure();
	void reactTemperature();
	void reactHumidity();
}
