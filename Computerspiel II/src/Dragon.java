
public class Dragon implements Vehicle {
	public int speed;
	public int health;
	public int damage;
	private String name;
	public Dragon(String pName) {
		speed = 150;
		health = 200;
		damage = 100;
		setName(pName);
	}
	@Override
	public void forward() {
		System.out.println("Vorw�rts!");
		
	}

	@Override
	public void left() {
		System.out.println("Nach links!");
		
	}

	@Override
	public void right() {
		System.out.println("Nach rechts!");
		
	}

	@Override
	public void backward() {
		System.out.println("Zur�ck!");
		
	}

	@Override
	public void jump() {
		System.out.println("Spring!");
		
	}
	public void setName(String pName) {
		name = pName;
	}
	public String getName() {
		return name;
	}

}
