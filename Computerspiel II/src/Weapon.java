public abstract class Weapon{
  private int maxAmmo;
  private int magazin;
  private String sound;
  private int damage;
  private int fireRate;
  private String title;
  public void setMaxAmmo(int pMax){
    maxAmmo = pMax;
  }
  public int getMaxAmmo(){
    return maxAmmo;
  }
  public void setMagazineSize(int pMagazin){
    magazin = pMagazin;
  }
  public int getMagazinSize(){
    return magazin;
  }
  public void setSound(String pSound){
    sound = pSound;
  }
  public void playSound(){
    System.out.println(sound);
  }
  public int getDamage() {
	  return damage;
  }
  public void setDamage(int pDamage) {
	  damage = pDamage;
  }
  public int getFireRate() {
	  return fireRate;
  }
  public void setFireRate(int pFireRate) {
	  fireRate = pFireRate;
  }
  public void setTitle(String pTitle) {
	  title = pTitle;
  }
  public String getTitle() {
	  return title;
  }
}
  