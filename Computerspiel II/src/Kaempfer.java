public class Kaempfer{
  private Weapon primary;
  private Weapon secondary;
  private Vehicle moveUtil;
  private String name;
  public Kaempfer(Weapon pPrimary, Weapon pSecondary, Vehicle pMove, String pName) {
	    setPrimary(pPrimary);
	    setSecondary(pSecondary);
	    setMoveUtil(pMove);
	    setName(pName);
	  }
  public void setPrimary(Weapon pPrimary){
    primary = pPrimary;
  }
  public void setSecondary(Weapon pSecondary){
    secondary = pSecondary;
  }
  public void setMoveUtil(Vehicle pMoveUtil){
    moveUtil = pMoveUtil;
  }
  public Weapon getPrimary() {
	  return primary;
  }
  public Weapon getSecondary() {
	  return secondary;
  }
  public Vehicle getMoveUtil() {
	  return moveUtil;
  }
  public void setName(String pName) {
	  name = pName;
  }
  public String getName() {
	  return name;
  }
}
    