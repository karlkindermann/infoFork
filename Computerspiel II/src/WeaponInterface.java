public interface WeaponInterface{
  String fire();
  void reload();
}