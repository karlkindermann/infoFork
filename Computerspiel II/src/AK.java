
public class AK extends Weapon {
	public AK() {
		setMaxAmmo(90);
		setMagazineSize(30);
		setSound("Rattattattat");
		setDamage(33);
		setFireRate(28);
		setTitle("AK-47");
	}
}
