
public class Stick extends Weapon {
	public Stick() {
		setMaxAmmo(9999);
		setMagazineSize(9999);
		setSound("Boff");
		setDamage(5);
		setFireRate(10);
		setTitle("Stock");
	}
}
