public interface Vehicle{
  void forward();
  void left();
  void right();
  void backward();
  void jump();
  String getName();
}