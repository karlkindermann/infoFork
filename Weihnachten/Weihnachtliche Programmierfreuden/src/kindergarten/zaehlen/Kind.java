package kindergarten.zaehlen;

public class Kind {
	private String name;
	private Integer number;
	public Kind(String name, Integer number) {
		this.number = number;
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	
}
