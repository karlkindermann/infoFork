package polnische.notation;

import java.util.HashMap;
import java.util.Map;

public class PostfixNotation {
	private Stack<Integer> charStack;
	private String postfix;
	private Map<String, Integer> opPrio = new HashMap<>();
	public PostfixNotation(String postfix) {
		this.postfix = postfix;
		this.charStack = new Stack<Integer>();
		opPrio.put("+", 1);
		opPrio.put("-", 1);
		opPrio.put("*", 2);
		opPrio.put("/", 2);
		opPrio.put("^", 3);
	}
	public String infixToPostfix(String infix) {
		String output = "";
		Stack<String> opStack = new Stack<String>();
		opStack.push("(");
		infix += ")";
		while(!opStack.isEmpty()) {
			String current = Character.toString(infix.charAt(0));
			System.out.println(current);
			try {
				Integer x = Integer.parseInt(current);
				output += x.toString();
			} catch (NumberFormatException nfe) {
				if (current.equals("(")) {
					opStack.push(current);
				} else if (this.opPrio.containsKey(current)) {
					String test = opStack.top();
					Integer prio1 = this.opPrio.get(test);
					Integer prio2 = this.opPrio.get(current);
					while(this.opPrio.containsKey(opStack.top()) && prio1 >= prio2){
						String op = opStack.top();
						output += op;
						opStack.pop();
						prio1 = this.opPrio.get(opStack.top());
					}
					opStack.push(current);
				} else if(current.equals(")")) {
					while(!opStack.top().equals("(")) {
						String op = opStack.top();
						output += op;
						opStack.pop();
					}
					opStack.pop();
				}
			}
			infix = infix.substring(1);
		}
		return output;
	}
	public void loese() {
		while(postfix.length() > 0) {
			String x = Character.toString(postfix.charAt(0));
			postfix = postfix.substring(1);
			try{
				int y = Integer.parseInt(x);
				charStack.push(y);
			} catch (NumberFormatException nfe){
				rechne(x);
			}
		}
		System.out.println("Das Ergebnis lautet: ");
		System.out.println(Integer.toString(charStack.top()));
	}
	private void rechne(String x){
		switch (x){
			case "+":
				add();
				break;
			case "*":
				multiply();
				break;
			case "/":
				divide();
				break;
			case "-":
				subtract();
				break;
			default:
				System.out.println("ungültiges zeichen");
				break;
		}
	}
	private void add(){
		Integer x = charStack.top();
		charStack.pop();
		Integer y = charStack.top();
		charStack.pop();
		Integer result = y + x;
		charStack.push(result);
	}
	private void multiply(){
		Integer x = charStack.top();
		charStack.pop();
		Integer y = charStack.top();
		charStack.pop();
		Integer result = y * x;
		charStack.push(result);
	}
	private void divide() {
		Integer x = charStack.top();
		charStack.pop();
		Integer y = charStack.top();
		charStack.pop();
		Integer result = y / x;
		charStack.push(result);
	}
	private void subtract(){
		Integer x = charStack.top();
		charStack.pop();
		Integer y = charStack.top();
		charStack.pop();
		Integer result = y - x;
		charStack.push(result);
	}
}
