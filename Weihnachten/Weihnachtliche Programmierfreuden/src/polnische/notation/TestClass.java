package polnische.notation;

public class TestClass {
    public static void main(String[] args) {
        PostfixNotation pN = new PostfixNotation("93-72+*5+");
        pN.loese();
        String postfix = pN.infixToPostfix("(9-3)*(7+2)+5");
        System.out.println(postfix);
    }
}
