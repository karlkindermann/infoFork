package patienten.liste;
public class PrioSchlange {
	private Queue<Patient> q1;
	private Queue<Patient> q2;
	private Queue<Patient> q3;
	public PrioSchlange() {
		q1 = new Queue<Patient>();
		q2 = new Queue<Patient>();
		q3 = new Queue<Patient>();
	}
	public void enqueue(Patient pPatient) {
		int prio = pPatient.getPrio();
		switch(prio) {
		case 1:
			q1.enqueue(pPatient);
			break;
		case 2:
			q2.enqueue(pPatient);
			break;
		case 3:
			q3.enqueue(pPatient);
			break;
		default:
			break;
		}
	}
	public Patient dequeue() {
		Patient p = q1.front();
		if (p == null) {
			p = q2.front();
			if (p == null) {
				p = q3.front();
				q3.dequeue();
			}else {q2.dequeue();}
		}else {q1.dequeue();}
		return p;
	}
	public int getLaenge() {
		int length = 0;
		length += getQueueLength(q1);
		length += getQueueLength(q2);
		length += getQueueLength(q3);
		return length;
		
	}
	private int getQueueLength(Queue<Patient> pQ) {
		int length = 0;
		Patient pat = (Patient) pQ.front();
		if (pat == null) {return 0;}
		else {
			pQ.enqueue(pat);
			length++;
			pQ.dequeue();
			while(pQ.front() != pat) {
				pQ.enqueue(pQ.front());
				length++;
				pQ.dequeue();
				
			}
		}
		return length;
	}
}
