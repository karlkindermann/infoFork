package patienten.liste;

public class Patient {
	private int prio;
	private String name;
	public Patient(int prio, String name) {
		this.prio = prio;
		this.name = name;
	}
	public int getPrio() {
		return prio;
	}
	public void setPrio(int prio) {
		this.prio = prio;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
