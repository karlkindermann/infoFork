package patienten.liste;

public class TestClass {

	public static void main(String[] args) {
		PrioSchlange prio = new PrioSchlange();
		Patient p1 = new Patient(1, "Kalle");
		Patient p2 = new Patient(2, "Sayan");
		Patient p3 = new Patient(3, "Robert");
		Patient p4 = new Patient(2, "Larry");
		prio.enqueue(p1);
		prio.enqueue(p2);
		prio.enqueue(p3);
		prio.enqueue(p4);
		System.out.println(prio.getLaenge());
		prio.dequeue();
		System.out.println(prio.getLaenge());

	}

}
