
public class Vocab {
private String origin, translation;
private boolean status;

public Vocab (String pOrigin, String pTranslation) {
	this.origin = pOrigin;
	this.translation = pTranslation;
	this.status = false;
}
public void setOrigin(String pOrigin) {
	this.origin = pOrigin;
}
public String getOrigin() {
	return this.origin;
}
public void setTranslation(String pTranslation) {
	this.translation = pTranslation;
}
public String getTranslation() {
	return this.translation;
}
public void setStatus(boolean pStatus) {
	this.status = pStatus;
}
public boolean getStatus() {
	return this.status;
}
}
