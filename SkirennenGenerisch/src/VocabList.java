import java.util.Scanner;

public class VocabList extends List<Vocab> {
	private Scanner scan;
	public VocabList() {
		this.scan = new Scanner(System.in);
	}
	public void startTraining(boolean onlyNew) {
		int counter = 0;
		int right = 0;
		while(this.isEmpty() != true && this.hasAccess()) {
			Vocab vocab = this.getContent();
			if(onlyNew && vocab.getStatus()) {continue;}
			String ask = vocab.getOrigin();
			System.out.println("Ausgangssprache:");
			System.out.println(ask);
			System.out.println("‹bersetzung");
			String answer = this.scan.next();
			if(answer.toLowerCase().equals("exit")) {
				break;
			}
			counter++;
			if(answer.toLowerCase().equals(vocab.getTranslation().toLowerCase())){
				vocab.setStatus(true);
				right++;
			}
			this.next();
		}
		System.out.println("Von " + counter + " Vokabeln hast du " + right + " richtig beantwortet!");
	}
	public void startTraining() {
		this.startTraining(false);
	}
	public void addVocab(String origin, String translation) {
		Vocab vocab = new Vocab(origin, translation);
		if(this.isEmpty()) {
			this.append(vocab);
			this.toFirst();
		}else {
			this.append(vocab);
		}
	}
	public void InsertionSort() {
		this.sort(this.first.getNextNode());
	}
	public void sort(ListNode current) {
		if (current == null) {
			return;
		}
		String value = (String) current.getContentObject().getOrigin();
		ListNode prev = this.getPrevious(current);
		ListNode next = current.getNextNode();
		this.current = current;
		remove();
		current.setNextNode(null);
		ListNode temp = current;
		while (prev != null) {
			temp = prev;
			String pValue = (String) prev.getContentObject().getOrigin();
			if (pValue.compareTo(value) > 0) {
				break;
			}
			prev = getPrevious(prev);
		}
		if (prev == null) {
			temp = prev;
		}
		this.insert(new Vocab(current.getContentObject().getOrigin(), current.getContentObject().getTranslation()));
		sort(next);
	}
	public void print() {
		ListNode current = this.first;
		while (current != null) {
			System.out.println(current.getContent());
			current = current.getNext();
		}
	}
}
