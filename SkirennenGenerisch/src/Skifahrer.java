public class Skifahrer {
	// Attribute
	String zName;
	int zZeit;

	// Konstruktor

	public Skifahrer(String pName, int pZeit) {
		zName = pName;
		zZeit = pZeit;
	}

	// Dienste
	/**
	 * nachher: Der Name des Abfahrtslaeufers wird geliefert.
	 */
	public String name() {
		return zName;
	}

	/**
	 * nachher: Die Laufzeit des Abfahrtslaeufers wurde geliefert.
	 */
	public int zeit() {
		return zZeit;
	}

	/**
	 * nachher: Die Daten des Abfahrtslaeufers wurden als Zeichenkette
	 * geliefert.
	 */
	public String toString() {
		String lDaten = zName + " ";
		if (zZeit != 0)
			lDaten = lDaten + ": " + zZeit;
		return lDaten;
	}
}
