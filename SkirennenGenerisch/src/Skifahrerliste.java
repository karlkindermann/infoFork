public class Skifahrerliste {
private List<Skifahrer> liste; 

	public Skifahrerliste() {
	    liste = new List<Skifahrer>();   
	}

	public void einsortieren(Skifahrer sf) {
	  liste.toFirst();
	  while (liste.hasAccess() && (liste.getContent().zeit() < sf.zeit())){
	    liste.next();
	  }
	  if (liste.hasAccess()){
	    liste.insert(sf);
	  } else {
	      liste.append(sf);
	    } 
	}
	
	public String ausgabe() {
		String erg = "";
		liste.toFirst();
		while (liste.hasAccess()) {
			erg += liste.getContent().toString();
			erg += "\n";
			liste.next();
        }
		return erg;
	}

}
